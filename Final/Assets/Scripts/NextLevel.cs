﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextLevel : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject == GameManager.instance.Player.gameObject)
        {
            GameManager.instance.loadlevel2();
        }
    }

}
