﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Block : MonoBehaviour {

    public GameObject Checkpoint;

    public Transform[] target;
    public float speed;

    public int current;


    void Update() {
        if (transform.position != target[current].position)
        {
            Vector3 pos = Vector3.MoveTowards(transform.position, target[current].position, speed * Time.deltaTime);
            GetComponent<Rigidbody2D>().MovePosition(pos);
           
        }
        else current = (current + 1) % target.Length;


    }




    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == GameManager.instance.Player.gameObject)
        {
            GameManager.instance.Player.transform.position = new Vector3(Checkpoint.transform.position.x, Checkpoint.transform.position.y, Checkpoint.transform.position.z);
        }
    }
}
