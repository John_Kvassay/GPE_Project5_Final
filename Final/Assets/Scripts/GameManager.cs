﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour {

    public static GameManager instance;
        public int tokensCollected;
        public GameObject Player;
    // Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void Awake() {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void loadMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void loadlevel1() {
        SceneManager.LoadScene("Level1");
    }

    public void loadlevel2() {
        SceneManager.LoadScene("Level2");
    }

    public void loadlevel3()
    {
        SceneManager.LoadScene("Level3");
    }

    public void YouWin()
    {
        SceneManager.LoadScene("YouWinScreen");
    }

    public void YouLose()
    {
        SceneManager.LoadScene("YouLoseScreen");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
